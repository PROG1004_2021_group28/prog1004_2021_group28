# PROG1004 2021 Group #28
This is ToDoVODO's repo for the course PROG1004 2021 group #28

## Note regarding the Wiki-to-PDF tool

We were not able to generate the Wiki-to-PDF following an update to the tool happening on 29.04.2021. Before this update we were able to generate the PDF. The files related to this tool has therefore been removed from the repo.