﻿#include <iostream>         //  cout, cin
#include <iomanip>          //  setw
#include <string>           //  string
#include <vector>           //  vector
#include <fstream>
#include "LesData2.h"       //  usefull library for reading user input
using namespace std;

const char persistentStorageFileName[] = "todo.dta";

enum taskPriority { Low, Normal, High };
enum taskStatus { Todo, Doing, Completed };


class Task
{
private:
	string title;
	string description;			// optional
	string deadline;			// optional
	enum taskPriority priority;
	enum taskStatus status;
public:
	Task() {}
	void changeStatus();		// change status
	void edit();				// edit task
	void readData();			// read user input for new task
	void printData();			// print task data
	void printTableRow(int nr); // print row to fit in table
	bool readFileStream(ifstream& fileInputStream); // Read from input file. Returns true if success, false if failure.
	string getTitle() { return title; }
	string getDescription() { return description; }
	string getDeadline() { return deadline; }
	taskPriority getPriority() { return priority; }
	taskStatus getStatus() { return status; }
};


class TodoList
{
private:
	vector<Task*> tasks;
public:
	bool readFromFile();
	bool writeToFile();
	void newTask();
	void printTodoList();
	void printTable();
	void printTask();
	void editTask();
	void changeTaskStatus();
	void deleteTask();
};


TodoList gList;

void printMenu();
void printEditMenu();
void help(char topic);
bool validateDateTime(string input);


int main()
{
	bool loadSuccess = gList.readFromFile();
	char command;

	cout << "88888888888           888              888     888              888"
		<< "\n    888               888              888     888              888"
		<< "\n    888  .d88b.   .d88888  .d88b.      Y88b   d88P .d88b.   .d88888  .d88b."
		<< "\n    888 d88''88b d88' 888 d88''88b      Y88b d88P d88''88b d88' 888 d88''88b"
		<< "\n    888 888  888 888  888 888  888       Y88o88P  888  888 888  888 888  888"
		<< "\n    888 Y88..88P Y88. 888 Y88..88P        Y888P   Y88..88P Y88. 888 Y88..88P"
		<< "\n    888  'Y88P'   'Y88888  'Y88P'          Y8P     'Y88P'   'Y88888  'Y88P'"
		<< "\n";

	printMenu();
	command = lesChar("\nCommand");

	while (command != 'Q')
	{
		switch (command) {
		case 'N':
			gList.newTask();
			break;
		case 'E':
			gList.editTask();
			break;
		case 'C':
			gList.changeTaskStatus();
			break;
		case 'D':
			gList.deleteTask();
			break;
		case 'L':
			gList.printTodoList();
			break;
		case 'S':
			gList.printTask();
			break;
		case 'M':
			printMenu();
			break;
		case 'H':
			help('M');
			break;
		default:
			cout << "\n\t'" << command << "' is not a valid command."
				<< "\n\tPlease enter the character of the command you would "
				<< "\n\tlike to execute or 'q' to quit and press enter\n";
			printMenu();
		}
		command = lesChar("\nCommand");
	}

	bool writeSuccess = gList.writeToFile();
	cout << "\n\tSaving todo list to file 'todo.dta'\n";

	return 0;
}

const int MAX_VALID_YR = 9999;
const int MIN_VALID_YR = 2021;


// Returns true if
// given year is valid.
bool isLeap(int year)
{
	// Return true if year
	// is a multiple pf 4 and
	// not multiple of 100.
	// OR year is multiple of 400.
	return (((year % 4 == 0) &&
		(year % 100 != 0)) ||
		(year % 400 == 0));
}


// Returns true if given
// year is valid or not.
bool isValidDate(int d, int m, int y)
{
	// If year, month and day
	// are not in given range
	if (y > MAX_VALID_YR || y < MIN_VALID_YR)
		return false;
	if (m < 1 || m > 12)
		return false;
	if (d < 1 || d > 31)
		return false;

	// Handle February month
	// with leap year
	if (m == 2) {
		if (isLeap(y))
			return (d <= 29);
		else
			return (d <= 28);
	}

	// Months of April, June,
	// Sept and Nov must have
	// number of days less than
	// or equal to 30.
	if (m == 4 || m == 6 || m == 9 || m == 11)
		return (d <= 30);

	return true;
}


bool validateDateTime(string input)
{
	if (input.empty())
		return false;

	// Split string into substrings with '/' as delimiter.
	// Check that each substring is valid.
	string substrings[3];
	int offset = 0;
	for (int i = 0; i < 3; i++)
	{
		// Find substring for integer
		auto slashPos = input.length();
		if (i < 2)
			slashPos = input.find_first_of('/', offset);

		// We couldn't find the delimiter. Invalid string.
		if (slashPos == string::npos) {
			cout << "\n\tInvalid input\n\tDeadline must me written as: dd/mm/yyyy";
			return false;
		}

		substrings[i] = input.substr(offset, slashPos - offset);
		offset = slashPos + 1;
	}
	// Validate day
	string dayString = substrings[0];
	int dayInt = 0;
	string monthString = substrings[1];
	int monthInt = 0;
	string yearString = substrings[2];
	int yearInt = 0;
	try
	{
		dayInt = stoi(dayString);
		monthInt = stoi(monthString);
		yearInt = stoi(yearString);
	}
	catch (exception&)
	{
		// We failed to turn any of the substrings into an integer, meaning we have invalid input.
		cout << "\n\tInvalid input\n\tDeadline must me written as: dd/mm/yyyy";
		return false;
	}

	if (dayString.empty() || dayString.length() > 2) {
		cout << "\n\tInvalid input\n\tDeadline must me written as: dd/mm/yyyy";
		return false;
	}
	if (dayInt < 1) {
		cout << "\n\tInvalid input\n\tThere is no " << dayInt << "th day";
		return false;
	}
	if (monthString.empty() || dayString.length() > 2) {
		cout << "\n\tInvalid input\n\tDeadline must me written as: dd/mm/yyyy";
		return false;
	}
	if (monthInt > 12 || monthInt < 1) {
		cout << "\n\tInvalid input\n\tThere is no " << monthInt << "th month";
		return false;
	}
	if (yearString.length() != 4) {
		cout << "\n\tInvalid input\n\tYear must be four didgets";
		return false;
	}
	if (yearInt < 2021) {
		cout << "\n\tInvalid input\n\t'" << yearInt << "' is in the past";
		return false;
	}
	if (!isValidDate(dayInt, monthInt, yearInt)) {
		cout << "\n\tInvalid input\n\tThe date " << input << " does not exist";
		return false;
	}
	return true;
}


bool TodoList::readFromFile()
{
	// Try opening our persistent storage file.
	// If we couldn't open it, we do nothing.
	// If it does exist, we try to load content from it.
	ifstream inFile(persistentStorageFileName);
	if (inFile.is_open())
	{
		// Load in tasks from file to gList
		bool success = true;
		while (success && !inFile.eof())
		{
			Task newTask = {};
			success = newTask.readFileStream(inFile);
			if (success)
				tasks.push_back(new Task(newTask));
			else
				return false;

			if (inFile.eof())
				break;
		}
	}
	return true;
}


bool TodoList::writeToFile()
{
	// Overwrite existing file completely, write all tasks to file.
	ofstream outFile(persistentStorageFileName, ofstream::trunc);

	if (!outFile.is_open())
		return false;

	for (Task* taskPtr : tasks)
	{
		const char newline = '\n';

		string temp;

		// Write the title
		temp = taskPtr->getTitle();
		outFile.write(temp.data(), temp.length());
		outFile.write(&newline, 1); // Write the newline symbol, meaning we start a new line.

		// Write the description
		temp = taskPtr->getDescription();
		outFile.write(temp.data(), temp.length());
		outFile.write(&newline, 1);

		// Write the deadline
		temp = taskPtr->getDeadline();
		if (!temp.empty())
		{
			// If not empty, split the string with '/' as delimiter
			// and print each substring on separate lines.
			int test = 0;
			for (int i = 0; i < 3; i++)
			{
				// Find substring for integer
				auto slashPos = temp.length();
				if (i < 2)
					slashPos = temp.find_first_of('/', test);
				string numberSubstring = temp.substr(test, slashPos - test);
				outFile.write(numberSubstring.data(), numberSubstring.length());
				outFile.write(&newline, 1);
				test = slashPos + 1;
			}
		}
		else
		{
			// If we have no deadline, just print 3 empty lines.
			for (int i = 0; i < 3; i++)
				outFile.write(&newline, 1);
		}

		// Write the priority
		temp = to_string((int)taskPtr->getPriority());
		outFile.write(temp.data(), temp.length());
		outFile.write(&newline, 1);

		// Write the status
		temp = to_string((int)taskPtr->getStatus());
		outFile.write(temp.data(), temp.length());
		outFile.write(&newline, 1);
	}
	return true;
}


/**
*   Change status.
*/
void TodoList::changeTaskStatus() {
	int taskNum;

	cout << "_______________________________________________________________________________\n"
		<< "\n  Change task status!\n";

	if (!tasks.empty())
	{
		printTable();
		taskNum = lesInt("\n  Which task would you like to change the status for?"
			"\n\n\tNr.", 1, tasks.size());
		if (taskNum >= 1 && taskNum <= tasks.size()) // number between 1...allTasks.
			tasks[taskNum - 1]->changeStatus();
	}
	else
		cout << "\n\tThere are no tasks in your todo list";

	cout << "\n\tTo show the menu again, type 'M' and press enter."
		<< "\n_______________________________________________________________________________\n";
}


/**
*   Change status.
*/
void Task::changeStatus() {

	char option;

	cout << "\n  You chose this task:\n";
	printData();

	do
	{
		option = lesChar("\n  Set new task status:"
			"\n\n\t(T)to-do / (D)doing / (C)completed");

		switch (option) {
		case 'T':
			status = Todo;
			break;
		case 'D':
			status = Doing;
			break;
		case 'C':
			status = Completed;
			break;
		default:
			cout << "\n\t'" << option << "' is not a valid input."
				<< "\n\tPlease enter 'T' for to-do, 'D' for doing or 'C' for completed.\n";
		}
	} while (option != 'T' && option != 'D' && option != 'C');

	cout << "\n\tTask updated!";
}


/**
*   Edit data if it is possible (and if the user wants)
*/
void Task::edit()
{
	char option;
	char pri;

	cout << "\n  You chose to edit this task:\n";
	printData();
	printEditMenu();

	do
	{
		option = lesChar("\n\tOption");
		switch (option) {
		case 'N':
			cout << "\n\tNew name: ";
			getline(cin, title);
			cout << "\n\tTask updated!";
			break;
		case 'P':
			do
			{
				pri = lesChar("\n  Enter new task priority:"
					"\n\n\t(L)low / (N)normal / (H)high");

				switch (pri) {
				case 'L':
					priority = Low;
					break;
				case 'N':
					priority = Normal;
					break;
				case 'H':
					priority = High;
					break;
				default:
					cout << "\n\t'" << option << "' is not a valid input."
						<< "\n\tPlease enter 'L' for low, 'N' for normal or 'H' for high.\n";
				}
			} while (pri != 'N' && pri != 'H' && pri != 'L');

			cout << "\n\tTask updated!";
			break;
		case 'D':
			cout << "\n\tEnter new description: ";
			getline(cin, description);
			cout << "\n\tTask updated!";
			break;
		case 'X':
			cout << "\n\tEnter new deadline (dd/mm/yyyy): ";
			getline(cin, deadline);
			while (!validateDateTime(deadline))
			{
				cout << "\n\n\tEnter deadline (dd/mm/yyyy): ";
				getline(cin, deadline);
			}
			cout << "\n\tTask updated!";
			break;
		case 'H':
			help('E');
			break;
		case 'M':
			break;
		default:
			cout << "\n\t'" << option << "' is not a valid input."
				<< "\n\tPlease enter one of the characters from the menu and press enter.\n";
			break;
		}
	} while (option != 'N' && option != 'P' && option != 'S'
		&& option != 'D' && option != 'X' && option != 'M');
}


/**
*   Read data from user
*/
void Task::readData()
{
	char option;

	// get task name
	do
	{
		cout << "\n\n  What would you like to name this task?"
			<< "\n\n\tName: ";
		getline(cin, title);

		if (title.length() > 53)
			cout << "\n\tError: Name too long (max 53 characters)";

	} while (title.length() > 53);

	// get priority
	do
	{
		option = lesChar("\n  Choose task priority:"
			"\n\n\t(L)low / (N)normal / (H)high");

		switch (option) {
		case 'L':
			priority = Low;
			break;
		case 'N':
			priority = Normal;
			break;
		case 'H':
			priority = High;
			break;
		default:
			cout << "\n\t'" << option << "' is not a valid input."
				<< "\n\tPlease enter 'L' for low, 'N' for normal or 'H' for high.\n";
		}
	} while (option != 'N' && option != 'H' && option != 'L');

	// get status
	do {
		option = lesChar("\n  Choose task status:"
			"\n\n\t(T)to-do / (D)doing / (C)completed");

		switch (option) {
		case 'T':
			status = Todo;
			break;
		case 'D':
			status = Doing;
			break;
		case 'C':
			status = Completed;
			break;
		default:
			cout << "\n\t'" << option << "' is not a valid input."
				<< "\n\tPlease enter 'T' for to-do, 'D' for doing or 'C' for completed.\n";
		}
	} while (option != 'T' && option != 'D' && option != 'C');

	// get description (optional)
	do
	{
		option = lesChar("\n  Would you like to add a description to this task?"
			"\n\n\t(Y)yes / (N)no");

		if (option != 'Y' && option != 'N')
			cout << "\n\t'" << option << "' is not a valid input."
			<< "\n\tPlease enter 'Y' for yes or 'N' for no.\n";

	} while (option != 'Y' && option != 'N');

	if (option == 'Y')
	{
		do
		{
			cout << "\n\tEnter description: ";
			getline(cin, description);

			if (description.length() > 500)
				cout << "\n\tError: Description too long (max 500 characters)";

		} while (description.length() > 500);
	}

	// get deadline (optional)
	do
	{
		option = lesChar("\n  Would you like to add a deadline to this task?"
			"\n\n\t(Y)yes / (N)no");

		if (option != 'Y' && option != 'N')
			cout << "\n\t'" << option << "' is not a valid input."
			<< "\n\tPlease enter 'Y' for yes or 'N' for no.\n";

	} while (option != 'Y' && option != 'N');

	if (option == 'Y')
	{
		cout << "\n\tEnter deadline (dd/mm/yyyy): ";
		getline(cin, deadline);
		while (!validateDateTime(deadline))
		{
			cout << "\n\n\tEnter deadline (dd/mm/yyyy): ";
			getline(cin, deadline);
		}
	}
	cout << "\n  NEW TASK CREATED:\n";
	printData();
}


/**
*   Create new task.
*
*   @see    Task::readData()
*/
void TodoList::newTask() {
	cout << "_______________________________________________________________________________\n"
		<< "\n  Create a new task!";

	Task* newTask = new Task;
	newTask->readData();
	tasks.push_back(newTask);

	cout << "\n\tTo show the menu again, type 'M' and press enter."
		<< "\n_______________________________________________________________________________\n";
}


/**
*   Prints data members
*/
void Task::printData() {

	string titlePad(53 - title.length(), ' ');
	string deadlinePad(53 - deadline.length(), ' ');

	int descriptionNumLines = description.length() / 53.f + 1;
	vector<string> descriptionArr;

	string priorityString[3] = { "Low", "Normal", "High" };
	string priorityWPad;
	string statusString[3] = { "To-do", "Doing", "Completed" };
	string statusWPad;

	for (int i = 0; i < descriptionNumLines; i++)
		descriptionArr.push_back(description.substr((i * 53) + i, (i + 1 * 53) - i));

	string descriptionPad(53 - descriptionArr[descriptionNumLines - 1].length(), ' ');


	switch (priority) {
	case Low:
		priorityWPad = priorityString[0].append(53 - priorityString[0].length(), ' ');
		break;
	case Normal:
		priorityWPad = priorityString[1].append(53 - priorityString[1].length(), ' ');
		break;
	case High:
		priorityWPad = priorityString[2].append(53 - priorityString[2].length(), ' ');
		break;
	default:
		priorityWPad = "Undefined";
		break;
	}

	switch (status) {
	case Todo:
		statusWPad = statusString[0].append(53 - statusString[0].length(), ' ');
		break;
	case Doing:
		statusWPad = statusString[1].append(53 - statusString[1].length(), ' ');
		break;
	case Completed:
		statusWPad = statusString[2].append(53 - statusString[2].length(), ' ');
		break;
	default:
		statusWPad = "Undefined";
		break;
	}

	cout << "   _________________________________________________________________________\n"
		<< "  |               |                                                         |\n"
		<< "  |  Name         |  " << title << titlePad << "  |\n"
		<< "  |_______________|_________________________________________________________|\n"
		<< "  |               |                                                         |\n"
		<< "  |  Priority     |  " << priorityWPad << "  |\n"
		<< "  |_______________|_________________________________________________________|\n"
		<< "  |               |                                                         |\n"
		<< "  |  Deadline     |  " << deadline << deadlinePad << "  |\n"
		<< "  |_______________|_________________________________________________________|\n"
		<< "  |               |                                                         |\n"
		<< "  |  Status       |  " << statusWPad << "  |\n"
		<< "  |_______________|_________________________________________________________|\n"
		<< "  |               |                                                         |\n";

	for (int y = 0; y < descriptionNumLines; y++)
	{
		if (descriptionNumLines == 1)
			cout << "  |  Description  |  " << descriptionArr[y] << descriptionPad << "  |\n";
		else if (y == 0)
			cout << "  |  Description  |  " << descriptionArr[y] << "  |\n";
		else if (y == descriptionNumLines - 1)
			cout << "  |               |  " << descriptionArr[y] << descriptionPad << "  |\n";
		else
			cout << "  |               |  " << descriptionArr[y] << "  |\n";
	}
	cout << "  |_______________|_________________________________________________________|\n";
}


/**
*   Print table row
*/
void Task::printTableRow(int nr)
{
	string titlePad;
	string titleForOutput;

	string deadlinePad(10 - deadline.length(), ' ');

	string priorityString[3] = { "Low", "Normal", "High" };
	string priorityWPad;

	string statusString[3] = { "To-do", "Doing", "Completed" };
	string statusWPad;

	if (title.length() <= 28)
	{
		titleForOutput = title;
		for (int i = 0; i < 28 - title.length(); i++)
			titlePad += ' ';
	}
	else
	{
		titleForOutput = title.substr(0, 25);
		titlePad = "...";
	}

	switch (priority) {
	case Low:
		priorityWPad = priorityString[0].append(9 - priorityString[0].length(), ' ');
		break;
	case Normal:
		priorityWPad = priorityString[1].append(9 - priorityString[1].length(), ' ');
		break;
	case High:
		priorityWPad = priorityString[2].append(9 - priorityString[2].length(), ' ');
		break;
	default:
		priorityWPad = "Undefined";
		break;
	}

	switch (status) {
	case Todo:
		statusWPad = statusString[0].append(9 - statusString[0].length(), ' ');
		break;
	case Doing:
		statusWPad = statusString[1].append(9 - statusString[1].length(), ' ');
		break;
	case Completed:
		statusWPad = statusString[2].append(9 - statusString[2].length(), ' ');
		break;
	default:
		statusWPad = "Undefined";
		break;
	}
	cout << "  |     |                              |           |            |           |\n"
		<< "  | " << setfill(' ') << setw(3) << nr << " | " << titleForOutput << titlePad << " | "
		<< priorityWPad << " | " << deadline << deadlinePad << " | " << statusWPad << " |\n"
		<< "  |_____|______________________________|___________|____________|___________|\n";
}


string loadNextFileLine(ifstream& fileInputStream)
{
	// Figure out which position we are in the input stream
	auto currentFileStreamPos = fileInputStream.tellg();
	// Then we jump until we find a newline
	fileInputStream.ignore(1000000, '\n');

	// If we hit end of file, return empty string
	if (fileInputStream.eof())
		return {};

	// Figure out the new position of the input stream, the difference between
	// this and the old position is the length of the line.
	auto positionOfNewline = fileInputStream.tellg();

	// We remove 2 to account for the newline symbol and get the correct size.
	auto lengthUntilNewline = positionOfNewline - currentFileStreamPos - 2;

	// Make a string with the correct line.
	string returnString;
	returnString.resize((int)lengthUntilNewline);

	// Move the stream back to where it was.
	fileInputStream.seekg(currentFileStreamPos);

	fileInputStream.read(&returnString[0], lengthUntilNewline);

	// Now we make the file input stream ignore the newline.
	fileInputStream.ignore(1);

	return returnString;
}


bool Task::readFileStream(ifstream& fileInputStream)
{
	// Load the title
	title = loadNextFileLine(fileInputStream);
	// We hit the end of file before we could grab other Task attributes, it's a corrupt file.
	if (title.empty() || fileInputStream.eof())
		return false;

	// Load the description
	description = loadNextFileLine(fileInputStream);
	// We hit the end of file before we could grab other Task attributes, it's a corrupt file.
	// We don't check for empty string because description is optional.
	if (fileInputStream.eof())
		return false;

	// Load the deadline if any
	string dayString = loadNextFileLine(fileInputStream);
	if (fileInputStream.eof())
		return false;
	string monthString = loadNextFileLine(fileInputStream);
	if (fileInputStream.eof())
		return false;
	string yearString = loadNextFileLine(fileInputStream);
	if (fileInputStream.eof())
		return false;
	// Check that all are either non-empty or all are empty.
	// If this is not true, the file is invalid.
	int counter = dayString.empty() + monthString.empty() + yearString.empty();
	if (counter != 0 && counter != 3)
		return false;

	if (!dayString.empty())
	{
		int dayInt = 0;
		int monthInt = 0;
		int yearInt = 0;
		try
		{
			dayInt = stoi(dayString);
			monthInt = stoi(monthString);
			yearInt = stoi(yearString);
		}
		catch (exception&)
		{
			return false;
		}
		deadline += to_string(dayInt) + "/";
		deadline += to_string(monthInt) + "/";
		deadline += to_string(yearInt);
	}

	// Load the priority string, cast it to enum.
	string priorityString = loadNextFileLine(fileInputStream);
	if (priorityString.empty() || fileInputStream.eof())
		return false;
	try
	{
		this->priority = (taskPriority)stoi(priorityString);
	}
	catch (exception& e)
	{
		// We were unable to turn the dayString into an integer. Corrupt file.
		return false;
	}

	// Load the status string, cast it to enum.
	string statusString = loadNextFileLine(fileInputStream);
	// Status is the last field, so we don't care if we're at the end-of-file
	try
	{
		status = (taskStatus)stoi(statusString);
	}
	catch (exception& e)
	{
		// We were unable to turn the dayString into an integer. Corrupt file.
		return false;
	}

	// We managed to parse everything without reaching end of file, Task successfully loaded.
	return true;
}


/**
*   Edit tasks title, deadline, priority, status
*
*   @see    TodoList::printTable()
*   @see    Task::edit()
*/
void TodoList::editTask()
{
	int taskNum;

	cout << "_______________________________________________________________________________\n"
		<< "\n  Edit task!\n";

	if (!tasks.empty())
	{
		printTable();

		taskNum = lesInt("\n  Which task would you like to edit?"
			"\n\n\tNr.", 1, tasks.size());

		if (taskNum >= 1 && taskNum <= tasks.size())	// number between 1 and number of tasks
			tasks[taskNum - 1]->edit();					// edit task
	}
	else
		cout << "\n\tThere are no tasks to edit!";

	cout << "\n\tTo show the menu again, type 'M' and press enter."
		<< "\n_______________________________________________________________________________\n";
}


/**
*   Delete a task
*/
void TodoList::deleteTask()
{
	int taskNum;
	char option;

	cout << "_______________________________________________________________________________\n"
		<< "\n  Delete task!\n";

	if (!tasks.empty())
	{
		printTable();

		taskNum = lesInt("\n  Which task would you like to delete?"
			"\n\n\tNr.", 1, tasks.size());

		do
		{
			option = lesChar("\n  Are you sure you want to delete this task?"
				"\n\n\t(Y)yes / (N)no");

			if (option != 'Y' && option != 'N')
				cout << "\n\t'" << option << "' is not a valid input."
				<< "\n\tPlease enter 'Y' for yes or 'N' for no.\n";

		} while (option != 'Y' && option != 'N');

		if (option == 'Y')
		{
			delete tasks[taskNum - 1];                      // deletes the current task!
			tasks[taskNum - 1] = tasks[tasks.size() - 1];   // At the back moves to the front
			tasks.pop_back();                               // deletes last pointr too.
			cout << "\n\tTask deleted!";
		}
		else
			cout << "\n\tTask was not deleted!";
	}
	else
		cout << "\n\tThere are no tasks to delete!";

	cout << "\n\tTo show the menu again, type 'M' and press enter.\n"
		<< "_______________________________________________________________________________\n";
}


/**
*   Print todo list
*/
void TodoList::printTodoList()
{
	cout << "_______________________________________________________________________________\n"
		<< "\n  TODO LIST:\n";
	printTable();
	cout << "\n\tTo show the menu again, type 'M' and press enter.\n"
		<< "_______________________________________________________________________________\n";
}


/**
*   Print todo list in a table
*/
void TodoList::printTable()
{
	if (!tasks.empty())
	{
		cout << "   _________________________________________________________________________\n"
			<< "  |     |                              |           |            |           |\n"
			<< "  | NR. | NAME                         | PRIORITY  | DEADLINE   | STATUS    |\n"
			<< "  |_____|______________________________|___________|____________|___________|\n";

		for (int i = 0; i < tasks.size(); i++)
			tasks[i]->printTableRow(i + 1);
	}
	else
		cout << "\n\tThere are no tasks in your to-do list";
}


/**
*   Print todo list
*/
void TodoList::printTask()
{
	int taskNum;

	cout << "_______________________________________________________________________________\n"
		<< "\n  Show one task in detail!\n";
	if (!tasks.empty())
	{
		printTable();

		taskNum = lesInt("\n  Witch task would you like to see?"
			"\n\n\tNr.", 1, tasks.size());

		if (taskNum >= 1 && taskNum <= tasks.size())	// number between 1 and number of tasks
			tasks[taskNum - 1]->printData();			// print task

	}
	else
		cout << "\n\tThere are no tasks in yout todo list!";

	cout << "\n\tTo show the menu again, type 'M' and press enter.\n"
		<< "_______________________________________________________________________________\n";
}


/**
*   Gives user instructions
*/
void help(char topic)
{
	cout << "_______________________________________________________________________________\n"
		<< "\n  TODO VODO HELP MANUAL"
		<< "\n  ---------------------"
		<< "\n  Todo Vodo is a to-do list application that can help you keep track of all"
		<< "\n  your tasks and deadlines. This help manual describes your options and how to"
		<< "\n  use the application.\n";

	switch (topic) {
	case 'M':
		cout << "\n  THE MENU"
			<< "\n  ---------------------"
			<< "\n  The menu displays your options. If the program is asking you for a command"
			<< "\n  like this:"
			<< "\n"
			<< "\n\tCommand: "
			<< "\n"
			<< "\n  it means you are currently in the main menu. To execute a command, enter the"
			<< "\n  character of the option and press enter. For example, the option to create a"
			<< "\n  new task is displayed in the menu as such:"
			<< "\n"
			<< "\n\tN - New task"
			<< "\n"
			<< "\n  To execute this command, type 'N' and press enter."
			<< "\n  To show the menu again, type 'M' and press enter."
			<< "\n_______________________________________________________________________________\n";
		break;
	case 'E':
		cout << "\n  EDIT TASK"
			<< "\n  ---------------------"
			<< "\n  To execute a option, enter the character of the option and press enter. For"
			<< "\n  example, the option to edit the name is displayed in the menu as such:"
			<< "\n"
			<< "\n\tN - Name"
			<< "\n"
			<< "\n  To execute this option, type 'N' and press enter."
			<< "\n  To go back to the main menu, type 'M' and press enter."
			<< "\n_______________________________________________________________________________\n";
		printEditMenu();
		break;
	}
}


/**
*   Prints out main menu options.
*/
void printMenu() {
	cout << "_______________________________________________________________________________\n"
		<< "\n  MENU:\n"
		<< "   __________________________________________________________\n"
		<< "  |                          |                               |\n"
		<< "  |  N - New task            |  L - Show todo list           |\n"
		<< "  |  E - Edit task           |  S - Show one task in detail  |\n"
		<< "  |  C - Change task status  |  M - Show the menu            |\n"
		<< "  |  D - Delete task         |  Q - Quit                     |\n"
		<< "  |__________________________|_______________________________|\n"
		<< "\n"
		<< "\tType 'H' and press enter to get help\n"
		<< "_______________________________________________________________________________\n";
}


/**
*   Prints out edit menu options.
*/
void printEditMenu() {
	cout << "\n  What would you like to edit?\n"
		<< "   _______________________________________________\n"
		<< "  |                   |                           |\n"
		<< "  |  N - Name         |  X - Deadline             |\n"
		<< "  |  P - Priority     |                           |\n"
		<< "  |  D - Description  |  M - Go back to main menu |\n"
		<< "  |___________________|___________________________|\n"
		<< "\n"
		<< "\tType 'H' and press enter to get help\n";
}